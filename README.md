Web Lab 17 &ndash; Deployments
==========
Hello this is Branch 1

Begin by forking this repository into your namespace by clicking the `fork` button above, then selecting your username from the resulting window. Once completed, click the `clone` button, copy the `Clone with HTTPS` value. Open IntelliJ, and from the welcome screen click `Check out from Version Control -> Git`, then paste the copied URL into the `URL` field of the resulting window. Provide your GitLab username and password if prompted.

Explore the files in the project, familiarizing yourself with the content.

When complete, demonstrate your code to your tutor. This must be verified with your tutor by the end of the week.


Exercise One &ndash; Deploying
----------
In this lab, you've been provided with a Servlet project which is fully functional when run on localhost (assuming your database is set up - make sure to run `scripts/db-init.sql` against your database first!). Begin by running the application and playing around with it locally to make sure it works for you. Also, read and understand the source code.

For this exercise, we will deploy the project as-is to the `sporadic` server. To do so, follow the instructions in [Deployments.md](Deployments.md). These instructions will be very useful when deploying other web apps too, such as your final project!


Exercise Two &ndash; Fixing Links
----------
Once you've deployed your project, you'll notice that it looks very wrong on the `sporadic.nz` server. This is due to some incorrect hyperlinks that have been used within the web app. In this exercise, you'll fix these links then redeploy, which will result in the deployed web app functioning correctly.

Looking at the homepage, it is apparent that the CSS file is not loading correctly, nor are the images for the existing articles. Let's examine why this is.

Looking at the `<link>` to the CSS file at the top of `article-list-view.jsp`, you'll see it points to `/css/site.css`. This is an *absolute* URL - the full path the server uses to retrieve the CSS file is obtained by concatenating the server name directly with this URL. For example, if our server's address is `localhost`, then the actual URL will be `https://localhost/css/site.css`. This works on our local machine because that's exactly the URL we're expecting, because our local development server doesn't have to account for multiple web apps being run on the server at the same time.

However, when we deploy to `sporadic.nz`, the URL will be `https://sporadic.nz/css/site.css`. This time, this URL is *incorrect* - it's missing our web app's name (our username combined with our deployed artifact name) - for example, `https://sporadic/nz/<your_upi>_lab-17-articles/css/site.css`.

To fix this, a naive approach would be to go through and prepend all our links in all our pages with our web app's name - however, if we do this, then the links would no longer work in our dev environment, nor if we decided to change server or web app name. A much better approach is to make sure *all* our links are *relative* links.

For example, in this case, change the link to `./css/site.css` (note the "." at the beginning which means "here" or "this directory"). This will work on both our local and remote servers.

For this exercise, fix all broken links in the web app, then redeploy to make sure it works.

**Hints & Notes:**
- Remember, if you're sure your web app has been fixed but things are still broken, try deleting the existing deployment first (see [Deployments.md](Deployments.md) for details).

- If you've already uploaded new images to the web app, then you delete your deployment, the uploaded images will be gone, but the database entries for the associated articles will still be there, resulting in broken links that you can't fix. If this happens, just re-run the database initialization script.

- You'll need to fix all links to other files within both `.jsp` files (i.e. all `src` and `href` attributes).

- You'll also need to fix any redirects (i.e. where `response.sendRedirect()` is used within the Servlet).

- You should **not** change the usage of the *request dispatchers* used to forward control from the servlets to the jsp files. Those links aren't subject to these same rules.

- You should **not** need to fix any links in `web.xml`.


(BONUS) Exercise Three &ndash; Have Fun
----------
For this exercise, extend the functionality of the web app in any way you see fit. Once your new functionality has been added and tested on `localhost`, make sure to redeploy to verify that the feature works in production.

Some ideas for possible features include (but are *not* limited to - use your imagination!:
- Add an *article detail* page allowing users to view the full text of articles when clicked. Shorten the text of each article which appears on the homepage, and just view the full text on a separate page.

- Allow users to delete articles.

- Implement a *search* function, allowing articles to be searched based on title and / or content.

- Allow articles to be dragged around on the homepage to change their order.

- Change the look & feel of the web app. Practice those CSS skills!

- Modify the web app to use AJAX to dynamically load articles.

- Anything else you'd like to do!