#### 1. Adding a new artifact ####
Hello World

The first thing you'll need to do is to add a new artifact to the project, which will be deployed to the server. While we've been running our project locally, we've used the *exploded WAR* artifact type. However, when deploying to a remote server we'll want to create an actual WAR file to deploy.

You need only perform this step once per project (i.e. you don't need to redo this step if you want to redeploy the app).

1. Begin by opening the module settings of your project.

   ![](./spec/screenshot-module-settings-menu.png)
   
2. Navigate to the *Artifacts* tab and click the *+* button to add a new Artifact.

   ![](./spec/screenshot-module-settings.png)
   
3. Select *Web Application: Archive | Empty* from the resulting pane.

   ![](./spec/screenshot-war-empty.png)
   
4. In the right-hand pane, start by giving the artifact a useful name. This should *not* include any spaces.

   ![](./spec/screenshot-name-war.png)
   
5. Right-click your project in the *available elements* pane, and select *"Put into output root*. Dismiss the window by hitting *apply*, then *OK*.

   ![](./spec/screenshot-output-root.png)

#### 2. Building a specific artifact ####
Before we can deploy an artifact, we need to build it. Luckily, the process is simple:

1. Begin by selecting *Build | Build Artifacts...* from the menu.

   ![](./spec/screenshot-build-artifacts-menu.png)
   
2. In the resulting pane, find the artifact you wish to build and select *Build* from the submenu.

   ![](./spec/screenshot-build-artifact.png)
   
3. After a brief delay, your new artifact should be visible in the `out` directory in your project.

   ![](./spec/screenshot-out-dir.png)
   
Remember to rebuild your artifact before you deploy it each time.

#### 3. Configuring a deployment ####
Our final step is to configure the deployment itself - that is, specify *what* is being deployed, *which* server to deploy to, and *where* on the server to deploy it.

1. Begin by selecting *Tools | Deployment | Configuration…* from the menu.

   ![](./spec/screenshot-configuration-menu.png)
   
2. In the resulting Deployment window, click the *+* button to add a new server. Select *SFTP* as the Type, and give the deployment a name.

   ![](./spec/screenshot-new-deployment.png)
   
3. In the *Connection* tab, configure the Host, Port, and Web server root URL exactly as shown here. Populate the Username and Password fields with the same username and password you were provided with for the Databases labs. Finally, fill the Root Path field with your username prefixed with a forward slash. Tick the *Save password* button so that you won’t be prompted each time.

   ```
   Host: sporadic.nz
   Port: 22
   User name: your username from the databases labs
   Authentication: Password
   Password: your password from the databases labs
   Root path: /your_user_name
   Web server URL: http://sporadic.nz
   ```
   
   ![](./spec/screenshot-deployment-configuration.png)
   
4. In the *Mappings* tab, set the *Local Path* Field to the output directory of your Web archive artifact. You can locate this easily by selecting the ellipsis button to the right of the field, and navigating down the directory tree. The *Deployment Path* should be a single forward slash. Modify the *Web path on server* field to be a forward slash, followed by your username, then an underscore, then your artifact name. Finally, click *OK*.

   ![](./spec/screenshot-mappings.png)
   
#### 4. Executing a deployment ####
Now that your deployment is configured, you can execute it by right-clicking on your generated WAR file artifact, and select *Deployment | Upload to ...*, where the *...* correspond to the name you gave to your deployment. If the console reports a successful deployment, then your project will be available at the URL shown in blue on the previous step.

![](./spec/screenshot-upload-to-deployment.png)

You should now be able to browse to your web app at:

```
https://sporadic.nz/<your-username>_<your-artifact-name>
```

If you get a *404* error, just wait a few seconds and try again.

#### 5. Updates & Troubleshooting ####
Usually, when you've made changes to your web app and wish to redeploy, all you *should* need to to is perform the *Executing a deployment* step above, once again. However, occasionally this might not work due to strange caching issues.

##### Clearing the browser cache #####

If you're not seeing the changes you've made after you've redeployed your web app, initally try waiting a few seconds and refreshing the page. If that doesn't work, then either your browser cache needs to be cleared or the server is (incorrectly) not regenerating your web app from the new WAR file.

To clear your browser cache, follow these instructions ([Chrome](https://support.google.com/accounts/answer/32050), [Firefox](https://kb.iu.edu/d/ahic#firefox)). Once this is done, refresh your page. If you're still not seeing your new content, you can try removing your web app from the server before redeploying (see below).

##### Deleting your deployment #####
If you need to delete your deployment, perform the following steps:

1. Open the *Remote Host* pane, by choosing *Tools | Deployment | Browse Remote Host* from the menu.

   ![](./spec/screenshot-browse-remote-host.png)
   
2. You should see a pane like the one below. From it, press the *Delete* button on your WAR file (or right-click it and choose *Delete* from the popup menu).

   ![](./spec/screenshot-remote-host-browser.png)
   
3. Confirm your web app has been deleted by browsing to it and refreshing the page till a *404* error appears.

4. Redeploy your web app by following the *Executing a deployment* step above.